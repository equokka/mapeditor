package org.academiadecodigo.maindalorians.equokka;

import org.academiadecodigo.simplegraphics.graphics.*;
import org.academiadecodigo.simplegraphics.keyboard.*;

import java.io.*;
import java.util.regex.Pattern;
import java.util.stream.IntStream;

public class MapEditor {
    // Structure
    public static final int PADDING = 30;
    public static final int CELL_SIZE = 20;
    private static final int WIDTH = 50;
    private static final int HEIGHT = 30;

    // Cursor
    private static Cursor cursor;

    // Statusline
    private static Text status;
    private static String last_action = "";

    // Grid struct
    private static final Grid map = new Grid(WIDTH, HEIGHT);

    // Keyboard ref
    private static Keyboard keyboard;

    // Save/load
    private static final String SAVEFILE = "save.txt";
    private static final String SAVEFORMAT = "%02d,%02d%n";
    private static final Pattern SAVEPATTERN = Pattern.compile("(\\d{2}),(\\d{2})");

    private MapEditor() {}

    public static void init() {
        // Because simplegraphics has no layer logic, the order of execution here matters.
        // If you draw A before B, A will be drawn behind B.
        // Layer-switching can be achieved in simplegraphics by redrawing the entire scene, however
        // that's an insane thing to do as fill() ops are very costly.

        // Force padding on the lower right
        Rectangle forcer = new Rectangle(
            PADDING + WIDTH * CELL_SIZE, PADDING + HEIGHT * CELL_SIZE, PADDING - 10, PADDING - 10);
        forcer.setColor(Color.WHITE);
        forcer.draw();

        // Map struct + cells
        map.draw();

        // Cursor init
        cursor = new Cursor(0, 0, map);

        // Frame
        Rectangle frame = new Rectangle(PADDING, PADDING, WIDTH * CELL_SIZE, HEIGHT * CELL_SIZE);
        frame.setColor(Color.BLACK);
        frame.draw();

        // Indexes
        IntStream.range(0, WIDTH)
            .mapToObj(i -> new Text(
                PADDING + i * CELL_SIZE + 2,
                PADDING - 15,
                String.format("%02d", i)
            )).forEach(num -> {
                num.setColor(Color.BLACK);
                num.draw();
            });
        IntStream.range(0, HEIGHT)
            .mapToObj(i -> new Text(
                PADDING - 15,
                PADDING + i * CELL_SIZE,
                String.format("%02d", i)
            )).forEach(num -> {
                num.setColor(Color.BLACK);
                num.draw();
            });

        // Index separators
        IntStream.rangeClosed(1, WIDTH)
            .mapToObj(i -> new Line(
                PADDING + i * CELL_SIZE,
                0,
                PADDING + i * CELL_SIZE,
                PADDING
            )).forEach(l -> {
                l.setColor(Color.BLACK);
                l.draw();
            });
        IntStream.rangeClosed(1, HEIGHT)
            .mapToObj(i -> new Line(
                0,
                PADDING + i * CELL_SIZE,
                PADDING,
                PADDING + i * CELL_SIZE
            )).forEach(l -> {
                l.setColor(Color.BLACK);
                l.draw();
            });

        // 0,0 Diagonal line
        Line l = new Line(0,0, PADDING, PADDING);
        l.setColor(Color.BLACK);
        l.draw();

        // Dots
        // TODO make this functional
        for (int i = 0; i <= WIDTH; i++) {
            for (int j = 0; j <= HEIGHT; j++) {
                Rectangle dot = new Rectangle(
                    PADDING + i * CELL_SIZE - 1,
                    PADDING + j * CELL_SIZE - 1,
                    3,
                    3
                );
                dot.setColor(Color.GRAY);
                dot.draw();
                dot.fill();
            }
        }

        // Status line init
        status = new Text(PADDING, PADDING + HEIGHT * CELL_SIZE + 5, "");
        status.draw();
        last_action = String.format("Opened new canvas. (%dx%d)", WIDTH, HEIGHT);
        update_status();

        // TODO refactor this into a keyboard controller or something.

        // Keyboard handling
        keyboard = new Keyboard(new KeyboardHandler() {
            @Override
            public void keyPressed(KeyboardEvent keyboardEvent) {
                switch (keyboardEvent.getKey()) {
                    case KeyboardEvent.KEY_DOWN  -> move_cursor( 0, +1);
                    case KeyboardEvent.KEY_UP    -> move_cursor( 0, -1);
                    case KeyboardEvent.KEY_LEFT  -> move_cursor(-1,  0);
                    case KeyboardEvent.KEY_RIGHT -> move_cursor(+1,  0);
                    case KeyboardEvent.KEY_SPACE -> paint();
                    case KeyboardEvent.KEY_C     -> clear();
                    case KeyboardEvent.KEY_S     -> save();
                    case KeyboardEvent.KEY_L     -> load();
                    case KeyboardEvent.KEY_Y     -> life_tick();
                }
            }

            @Override
            public void keyReleased(KeyboardEvent keyboardEvent) {}
        });

        register_key(KeyboardEvent.KEY_DOWN);
        register_key(KeyboardEvent.KEY_UP);
        register_key(KeyboardEvent.KEY_LEFT);
        register_key(KeyboardEvent.KEY_RIGHT);
        register_key(KeyboardEvent.KEY_SPACE);
        register_key(KeyboardEvent.KEY_C);
        register_key(KeyboardEvent.KEY_S);
        register_key(KeyboardEvent.KEY_L);
        register_key(KeyboardEvent.KEY_Y);
    }

    private static void register_key(int keyboardEvent) {
        KeyboardEvent k = new KeyboardEvent();
        k.setKeyboardEventType(KeyboardEventType.KEY_PRESSED);
        k.setKey(keyboardEvent);
        keyboard.addEventListener(k);
    }

    private static void update_status() {
        status.setText(String.format("[%02d,%02d] %s", cursor.getX(), cursor.getY(), last_action));
    }

    private static void move_cursor(int x, int y) {
        cursor.move(x, y);

        update_status();
    }

    private static void paint() {
        map.get(cursor.getX(), cursor.getY()).toggle();
        cursor.redraw();

        last_action = "";
        update_status();
    }

    private static void clear() {
        map.clear();
        cursor.redraw();

        last_action = "Cleared the canvas.";
        update_status();
    }

    private static void life_tick() {
        map.lifeTick();
        cursor.redraw();

        last_action = "Did a Life sim step.";
        update_status();
    }

    private static void save() {
        try (BufferedWriter fw = new BufferedWriter(new FileWriter(SAVEFILE))) {
            // TODO make this functional
            for (int i = 0; i < WIDTH; i++) {
                for (int j = 0; j < HEIGHT; j++) {
                    if (map.get(i, j).isPainted()) {
                        fw.write(String.format(SAVEFORMAT, i, j));
                    }
                }
            }
            fw.flush();
        }
        catch (IOException e) {
            e.printStackTrace();
        }

        last_action = "Saved canvas to disk.";
        update_status();
    }

    private static void load() {
        clear();

        try (BufferedReader fr = new BufferedReader(new FileReader(SAVEFILE))) {
            fr.lines()
                .map(SAVEPATTERN::matcher).forEach(m -> {
                    // I need the side-effect of this, not the value it returns.
                    //noinspection ResultOfMethodCallIgnored
                    m.find();
                    map.get(Integer.parseInt(m.group(1)), Integer.parseInt(m.group(2))).paint();
                });
        }
        catch (IOException e) {
            e.printStackTrace();
        }

        last_action = "Loaded canvas from disk.";
        update_status();
    }
}
