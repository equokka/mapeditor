package org.academiadecodigo.maindalorians.equokka;

import org.academiadecodigo.simplegraphics.graphics.Color;
import org.academiadecodigo.simplegraphics.graphics.Rectangle;

public class Cell {
    private final int x;
    private final int y;
    private Rectangle rect;
    private boolean painted = false;
    private final Grid grid;

    public Cell(int x, int y, Grid grid) {
        this.x = x;
        this.y = y;
        this.grid = grid;
    }

    public int getX() {
        return x;
    }
    public int getY() {
        return y;
    }
    public boolean isPainted() {
        return painted;
    }

    public boolean toggle() {
        painted = !painted;
        redraw();
        return painted;
    }

    // MAJOR performance increase from checking if something's already drawn before redrawing it.
    // This is because fill() is slow

    public void paint () {
        if (painted) return;
        painted = true;
        redraw();
    }
    public void unpaint() {
        if (!painted) return;
        painted = false;
        redraw();
    }

    public int getNumOfNeighbors() {
        int h = grid.getHeight();
        int w = grid.getWidth();
        int num = 0;

        // I'm sorry for this nightmare
        // But it helps to think about this as a table
        if (x != 0                  ) if (grid.get(x - 1, y    ).isPainted()) num++;
        if (x != 0     && y != 0    ) if (grid.get(x - 1, y - 1).isPainted()) num++;
        if (x != 0     && y != h - 1) if (grid.get(x - 1, y + 1).isPainted()) num++;
        if (x != w - 1              ) if (grid.get(x + 1, y    ).isPainted()) num++;
        if (x != w - 1 && y != 0    ) if (grid.get(x + 1, y - 1).isPainted()) num++;
        if (x != w - 1 && y != h - 1) if (grid.get(x + 1, y + 1).isPainted()) num++;
        if (              y != 0    ) if (grid.get(x,     y - 1).isPainted()) num++;
        if (              y != h - 1) if (grid.get(x,     y + 1).isPainted()) num++;

        return num;
    }

    public void invalidate() {
        if (rect != null) rect.delete();
    }

    public void redraw() {
        if (rect == null) {
            rect = new Rectangle(
                MapEditor.PADDING + x * MapEditor.CELL_SIZE,
                MapEditor.PADDING + y * MapEditor.CELL_SIZE,
                MapEditor.CELL_SIZE,
                MapEditor.CELL_SIZE);
            rect.draw();
        }
        rect.setColor(painted ? Color.BLACK : Color.WHITE);
        rect.fill();
    }
}
