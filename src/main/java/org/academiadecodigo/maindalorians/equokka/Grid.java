package org.academiadecodigo.maindalorians.equokka;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

public class Grid {
    private final int width;
    private final int height;
    private final List<List<Cell>> grid = new ArrayList<>();

    public Grid(int width, int height) {
        this.width = width;
        this.height = height;
        // TODO make this functional
        for (int i = 0; i < width; i++) {
            grid.add(new ArrayList<>());
            for (int j = 0; j < height; j++) {
                grid.get(i).add(new Cell(i, j, this));
            }
        }
    }

    public int getWidth() {
        return width;
    }
    public int getHeight() {
        return height;
    }

    public Cell get(int x, int y) {
        return grid.get(x).get(y);
    }

    public void clear() {
        grid.stream()
            .flatMap(Collection::stream)
            .forEach(Cell::unpaint);
    }

    public void invalidate() {
        grid.stream()
            .flatMap(Collection::stream)
            .forEach(Cell::invalidate);
    }

    public void draw() {
        grid.stream()
            .flatMap(Collection::stream)
            .forEach(Cell::redraw);
    }

    public void lifeTick() {
        // TODO make this functional

        // See https://en.wikipedia.org/wiki/Conway%27s_Game_of_Life#Rules
        // 1. Any live cell with two or three live neighbours survives.
        // 2. Any dead cell with three live neighbours becomes a live cell.
        // 3. All other live cells die in the next generation.
        // Similarly, all other dead cells stay dead.

        boolean[][] g = new boolean[width][height];

        for (int i = 0; i < width; i++) {
            for (int j = 0; j < height; j++) {
                int n = get(i, j).getNumOfNeighbors();
                boolean p = get(i, j).isPainted();

                // Start with the current state
                g[i][j] = p;

                // Live cells with a number of neighbors different from 2 or 3 die
                if (p && !(n == 2 || n == 3)) g[i][j] = false;

                // Dead cells with 3 neighbors become live
                if (!p && n == 3) g[i][j] = true;
            }
        }

        // All other cells die
        clear();

        for (int i = 0; i < width; i++) {
            for (int j = 0; j < height; j++) {
                if (g[i][j]) get(i, j).paint();
            }
        }
    }
}
