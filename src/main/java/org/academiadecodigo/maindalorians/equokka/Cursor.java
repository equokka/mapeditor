package org.academiadecodigo.maindalorians.equokka;

import org.academiadecodigo.simplegraphics.graphics.Color;
import org.academiadecodigo.simplegraphics.graphics.Rectangle;

import static org.academiadecodigo.maindalorians.equokka.MapEditor.CELL_SIZE;
import static org.academiadecodigo.maindalorians.equokka.MapEditor.PADDING;

public class Cursor {
    private int x;
    private int y;
    private final Grid grid;
    private final Rectangle rect;

    public Cursor(int x, int y, Grid grid) {
        this.x = x;
        this.y = y;
        this.grid = grid;
        rect = new Rectangle(PADDING, PADDING, CELL_SIZE >> 1, CELL_SIZE >> 1);
        rect.setColor(Color.YELLOW);
        redraw();
    }

    public int getX() {
        return x;
    }
    public int getY() {
        return y;
    }

    public void move(int dx, int dy) {
        if (
            x + dx >= grid.getWidth() || y + dy >= grid.getHeight() ||
            x + dx < 0                || y + dy < 0
        ) return;

        rect.translate(dx * CELL_SIZE, dy * CELL_SIZE);
        x += dx;
        y += dy;

        redraw();
    }

    public void redraw() {
        rect.setColor(grid.get(x, y).isPainted() ? Color.YELLOW : Color.BLUE);
        rect.fill();
    }
}
