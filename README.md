# MapEditor

by equokka aka Daniel Conceição

---

I think it's a pretty good solution on its own but I'm mostly happy about the [Conway's Game of Life](https://en.wikipedia.org/wiki/Conway%27s_Game_of_Life) simulation in it.


### Controls

| Key                                              | Action             |
| :-:                                              | :-                 |
| <kbd>←</kbd><kbd>↑</kbd><kbd>→</kbd><kbd>↓</kbd> | Movement           |
| <kbd>Space</kbd>                                 | Paint              |
| <kbd>C</kbd>                                     | Clear              |
| <kbd>S</kbd>                                     | Save               |
| <kbd>L</kbd>                                     | Load               |
| <kbd>Y</kbd>                                     | Do a Life sim tick |
